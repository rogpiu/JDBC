
public class Materia {
 
	private String materia;
	private String areaConhecimento;
	
	public String getMateria() {
		return materia;
	}
	public void setMateria(String materia) {
		this.materia = materia;
	}
	public String getAreaConhecimento() {
		return areaConhecimento;
	}
	public void setAreaConhecimento(String areaConhecimento) {
		this.areaConhecimento = areaConhecimento;
	}
	@Override
	public String toString() {
		return "Materia [materia=" + materia + ", areaConhecimento=" + areaConhecimento + "]";
	}
	
	
	
	
}
