import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;



public class ClienteDao {

	private Connection conexao;
	private ArrayList<Materia> materias;
	
	public ClienteDao() {
		this.conexao = Conexao.getConnection();
	}
	
	public void inserir(Materia materia) {
		String query = "insert into materia (materia, area_conhecimento)" +
				"values (?,?)";
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, materia.getMateria());
			statement.setString(2, materia.getAreaConhecimento());

			statement.execute();
			statement.close();

		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public ArrayList<Materia> consultar(){
		String query = "select * from materia";
		ArrayList<Materia> materias = new ArrayList<Materia>();
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				Materia materia = new Materia();
				materia.setMateria(rs.getString("materia"));
				materia.setAreaConhecimento(rs.getString("area_conhecimento"));
				materias.add(materia);
				
			}
			statement.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return materias;
	}
	
	public ArrayList<Materia> consultarAreaConhecimento(String areaConhecimento){
		String query = "select * from materia where area_conhecimento = ?";
		ArrayList<Materia> materias = new ArrayList<Materia>();
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, areaConhecimento);
			ResultSet rs = statement.executeQuery();

			while(rs.next()) {
				Materia materia = new Materia();
				materia.setMateria(rs.getString("materia"));
				materia.setAreaConhecimento(rs.getString("area_conhecimento"));
				materias.add(materia);
				
			}
			statement.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return materias;
	}
	
	public void deletar(Materia materia) {
		String query = "delete from materia where materia = ?";
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, materia.getMateria());
			statement.execute();
			statement.close();
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	public void atualizar(Materia materia) {
		String query = "update materia set area_conhecimento = ? where materia = ?";
//		String query = "update materia set materia = ? where materia = ?";
		
		try {
			PreparedStatement statement = conexao.prepareStatement(query);
			statement.setString(1, materia.getAreaConhecimento());
			statement.setString(2, materia.getMateria());
			statement.execute();
			statement.close();
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
