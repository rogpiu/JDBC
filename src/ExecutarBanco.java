import java.util.ArrayList;

public class ExecutarBanco {

	public static void main(String[] args) {
		
		Materia fisica = new Materia();
		ArrayList<Materia> materias;
		
		fisica.setMateria("Geografia");
		fisica.setAreaConhecimento("Biologicas");
		
		
		ClienteDao dao = new ClienteDao();
//		dao.deletar(fisica);
//		
//		dao.inserir(fisica);
//		materias = dao.consultarAreaConhecimento("Exatas");
		dao.atualizar(fisica);
		materias = dao.consultar();
		
		for(Materia materia: materias) {
			System.out.println(materia);
		}
		
	}

}
